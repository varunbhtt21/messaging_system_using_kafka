# from confluent_kafka import Producer
 
# p = Producer({'bootstrap.servers': 'localhost:9092'})
# p.produce('mytopic', key='hello', value='world')
# p.flush(30)
from confluent_kafka import Producer
from confluent_kafka import Consumer, KafkaError
import threading 

settings = {
    'bootstrap.servers': 'localhost:9092',
    'group.id': 'mygroup',
    'client.id': 'client-1',
    'enable.auto.commit': True,
    'session.timeout.ms': 6000,
    'default.topic.config': {'auto.offset.reset': 'smallest'}
}

p = Producer({'bootstrap.servers': 'localhost:9092'})



def acked(err, msg):
    if err is not None:
        print("Failed to deliver message: {0}: {1}"
              .format(msg.value(), err.str()))
    else:
        print("Message produced: {0}".format(msg.value()))



def producer(val):

    try:
        # print("haan",'myvalue #{0}'.format(val))
        p.produce('mytopic', val, callback=acked)
        p.poll(0.5)

    except KeyboardInterrupt:
        pass

    p.flush(30)






def consumer():
    c = Consumer(settings)

    c.subscribe(['mytopic1'])

    try:
        while True:
            msg = c.poll(0.1)
            if msg is None:
                continue

            elif not msg.error():
                print('Received message: {0}'.format(msg.value()))
                check = msg.value()
                check = check.decode("utf-8").split("$")[0]

                if check == "varun":
                    val = "1"
                    t1 = threading.Thread(target=producer, args=(val,))
                    t1.start() 

                break
            elif msg.error().code() == KafkaError._PARTITION_EOF:
                print('End of partition reached {0}/{1}'
                      .format(msg.topic(), msg.partition()))
            else:
                print('Error occured: {0}'.format(msg.error().str()))
       

    except KeyboardInterrupt:
        pass

    finally:
        c.close()




while(1):
    # t1 = threading.Thread(target=producer, args=()) 
    t2 = threading.Thread(target=consumer, args=()) 

    # t1.start()
    t2.start()

    # t1.join(5)
    t2.join(5)

print("done")
