import threading , queue
from confluent_kafka import Consumer, KafkaError
from confluent_kafka import Producer


q = queue.Queue()

settings = {}
status = True

def acked(err, msg):
	if err is not None:
		print("Failed to deliver message: {0}: {1}"
			  .format(msg.value(), err.str()))
	else:
		pass
		# print("Message produced: {0}".format(msg.value()))



def producer(topic, file):

	global status

	while 1:
		inp = input(">> ")

		if inp == 'q':
			status = False
			break

		file.write(topic + " : "+ inp +"\n")
		p = Producer({'bootstrap.servers': 'localhost:9092'})
		p.produce(topic, inp, callback=acked)
			
		p.flush(30)



def consumer(topic, file):
	c = Consumer(settings)
	c.subscribe([topic])

	
	while status:

		msg = c.poll(0.1)

		if msg is None:
			continue

		elif not msg.error():
			# print('Received message: {0}'.format(msg.value()))
			o = msg.value().decode("utf-8") 
			
			print(">> ",o)
			file.write(topic + " : "+ str(o) +"\n")

		elif msg.error().code() == KafkaError._PARTITION_EOF:
			print('End of partition reached {0}/{1}'
				  .format(msg.topic(), msg.partition()))
		else:
			print('Error occured: {0}'.format(msg.error().str()))
	

	c.close()



def Call_Threads(inp):
	out = ""
	topic = "chat"
	t1 = threading.Thread(target=producer, args=(inp,)) 
	t2 = threading.Thread(target=consumer, args=()) 


	t1.start()
	t2.start()

	t1.join()
	t2.join()




def check_login():
	login_id = input("Login ID : ")
	login_id = login_id + "$" + "1"
	verify = Call_Threads(login_id)

	if verify == "1":
		print("Valid User")
	else:
		print("Invalid User")



chat_history = {}

def chatting():
	your_id = input("Your ID : ")

	global settings
	settings = {
	'bootstrap.servers': 'localhost:9092',
	'group.id': your_id,
	'enable.auto.commit': True,
	'session.timeout.ms': 6000,
	'default.topic.config': {'auto.offset.reset': 'earliest'}
	}

	user_id = input("Friends ID : ")

	chat_name = your_id+"_"+user_id+".txt"

	try:
		if chat_history[chat_name]:
			file_read = open(chat_name, 'r')
			print(file_read)
			print("\n")

	except:
		chat_history[chat_name] = 1
		file1 = open(chat_name, 'a+')
		file1.write(chat_name+"\n")
		file1.close()


	file = open(chat_name, 'a+')


	
	t2 = threading.Thread(target=consumer, args=(your_id,file,))
	t1 = threading.Thread(target=producer, args=(user_id,file,)) 
	
	t2.start()
	t1.start()	


	t1.join()
	t2.join()

	file.close()


check_login()

	




